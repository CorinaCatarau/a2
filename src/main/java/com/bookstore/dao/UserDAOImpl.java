package com.bookstore.dao;

import com.bookstore.model.ListUser;
import com.bookstore.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Repository;

import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Corina on 24.04.2016.
 */
@Repository
public class UserDAOImpl extends GenericDAOImpl<ListUser,Integer> implements UserDAO {
    private static final String USER_XML="D:\\SD2016_30434_Catarau_Corina_Bookstore\\src\\users.xml";
    @Autowired
    private Jaxb2Marshaller marshaller;

    @Override
    public void delete(String id, String xmlFile) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(xmlFile);
            ListUser listUser= (ListUser) marshaller.unmarshal(new StreamSource(fis));
            for(int i=0;i<listUser.getUserList().size();i++){
                if(listUser.getUserList().get(i).getId().equals(id)){
                    listUser.getUserList().remove(i);
                }
            }
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(xmlFile);
                marshaller.marshal(listUser, new StreamResult(fos));
            } finally {
                fos.close();
            }
        } finally {
            fis.close();
        }

    }


    @Override
    public ListUser update(ListUser object) throws FileNotFoundException {

        return null;
    }

    @Override
    public User getUserById(String id) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(USER_XML);
            ListUser listUser= (ListUser) marshaller.unmarshal(new StreamSource(fis));
            for(int i=0;i<listUser.getUserList().size();i++){
                if(listUser.getUserList().get(i).getId().equals(id)){
                    return listUser.getUserList().get(i);
                }
            }

        } finally {
            fis.close();
        }
        return  null;


    }

    @Override
    public User getUserByName(String  name) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(USER_XML);
            ListUser listUser= (ListUser) marshaller.unmarshal(new StreamSource(fis));
            for(int i=0;i<listUser.getUserList().size();i++){
                if(listUser.getUserList().get(i).getName().equals(name)){
                    return listUser.getUserList().get(i);
                }
            }

        } finally {
            fis.close();
        }
        return  null;

    }
}
