package com.bookstore.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by Corina on 24.04.2016.
 */
public class GenericDAOImpl<T,K> implements GenericDAO<T,K> {
    @Autowired
    private Jaxb2Marshaller marshaller;

    @Override
    public void create(T object, String xmlFile) throws IOException, JAXBException {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(xmlFile);
            marshaller.marshal(object, new StreamResult(fos));
        } finally {
            fos.close();
        }

    }

    @Override
    public void delete(String id, String xmlFile) throws IOException {


    }

    @Override
    public T update(T object) throws FileNotFoundException {
        return null;
    }

    @Override
    public T getAll(String xmlFile) throws IOException, JAXBException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(xmlFile);
            return (T)marshaller.unmarshal(new StreamSource(fis));
        } finally {
            fis.close();
        }
    }
}
