package com.bookstore.dao;

import com.bookstore.model.Book;
import com.bookstore.model.ListBook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Repository;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 24.04.2016.
 */
@Repository
public class BookDAOImpl extends GenericDAOImpl<ListBook,String> implements  BookDAO{
    private static final String BOOK_XML="D:\\SD2016_30434_Catarau_Corina_Bookstore\\src\\books.xml";
    @Autowired
    private Jaxb2Marshaller marshaller;

    @Override
    public void delete(String id, String xmlFile) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(xmlFile);
            ListBook listBook= (ListBook) marshaller.unmarshal(new StreamSource(fis));
            for(int i=0;i<listBook.getBookList().size();i++){
                if(listBook.getBookList().get(i).getId().equals(id)){
                    listBook.getBookList().remove(i);
                }
            }
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(xmlFile);
                marshaller.marshal(listBook, new StreamResult(fos));
            } finally {
                fos.close();
            }
        } finally {
            fis.close();
        }

    }

    @Override
    public Book getBookById(String id) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(BOOK_XML);
            ListBook listBook= (ListBook) marshaller.unmarshal(new StreamSource(fis));
            for(int i=0;i<listBook.getBookList().size();i++){
                if(listBook.getBookList().get(i).getId().equals(id)){
                    return listBook.getBookList().get(i);
                }
            }

        } finally {
            fis.close();
        }
        return  null;

    }

    @Override
    public List<Book> getBookByName(String name) throws IOException {
        FileInputStream fis = null;
        List<Book> booksFound=new ArrayList<Book>();
        try {
            fis = new FileInputStream(BOOK_XML);
            ListBook listBook= (ListBook) marshaller.unmarshal(new StreamSource(fis));
            for(int i=0;i<listBook.getBookList().size();i++){
                if(listBook.getBookList().get(i).getTitle().equals(name)){
                    booksFound.add(listBook.getBookList().get(i));
                }
            }
            return  booksFound;

        } finally {
            fis.close();
        }

    }

    @Override
    public List<Book> getBooksByGenre(String genre) throws IOException {
        FileInputStream fis = null;
        List<Book> booksFound=new ArrayList<Book>();
        try {
            fis = new FileInputStream(BOOK_XML);
            ListBook listBook= (ListBook) marshaller.unmarshal(new StreamSource(fis));
            for(int i=0;i<listBook.getBookList().size();i++){
                if(listBook.getBookList().get(i).getGenre().equals(genre)){
                    booksFound.add(listBook.getBookList().get(i));
                }
            }
            return  booksFound;

        } finally {
            fis.close();
        }
    }

    @Override
    public List<Book> getBooksByPriceRange(int startPrice, int endPrice) throws IOException {
        FileInputStream fis = null;
        List<Book> booksFound=new ArrayList<Book>();
        try {
            fis = new FileInputStream(BOOK_XML);
            ListBook listBook= (ListBook) marshaller.unmarshal(new StreamSource(fis));
            if(startPrice!=0&&endPrice!=0) {
                for (int i = 0; i < listBook.getBookList().size(); i++) {
                    if ((listBook.getBookList().get(i).getPrice() >= startPrice) && (listBook.getBookList().get(i).getPrice() <= endPrice)) {
                        booksFound.add(listBook.getBookList().get(i));
                    }
                }
            }else if(startPrice==0){
                for (int i = 0; i < listBook.getBookList().size(); i++) {
                    if (listBook.getBookList().get(i).getPrice() <= endPrice) {
                        booksFound.add(listBook.getBookList().get(i));
                    }
                }
            }else if(endPrice==0) {
                for (int i = 0; i < listBook.getBookList().size(); i++) {
                    if (listBook.getBookList().get(i).getPrice() >= startPrice) {
                        booksFound.add(listBook.getBookList().get(i));
                    }
                }
            }
            return  booksFound;

        } finally {
            fis.close();
        }
    }

    @Override
    public List<Book> getBooksByAuthor(String author) throws IOException {
        FileInputStream fis = null;
        List<Book> booksFound=new ArrayList<Book>();
        try {
            fis = new FileInputStream(BOOK_XML);
            ListBook listBook= (ListBook) marshaller.unmarshal(new StreamSource(fis));
            for(int i=0;i<listBook.getBookList().size();i++){
                if(listBook.getBookList().get(i).getAuthor().equals(author)){
                    booksFound.add(listBook.getBookList().get(i));
                }
            }
            return  booksFound;

        } finally {
            fis.close();
        }
    }

    @Override
    public List<Book> getBooksCheaperThan(int price) throws IOException {
        FileInputStream fis = null;
        List<Book> booksFound=new ArrayList<Book>();
        try {
            fis = new FileInputStream(BOOK_XML);
            ListBook listBook= (ListBook) marshaller.unmarshal(new StreamSource(fis));
            for(int i=0;i<listBook.getBookList().size();i++){
                if((listBook.getBookList().get(i).getPrice()<=price)){
                    booksFound.add(listBook.getBookList().get(i));
                }
            }
            return  booksFound;

        } finally {
            fis.close();
        }
    }

    @Override
    public List<Book> getBooksFrom(int price) throws IOException {
        FileInputStream fis = null;
        List<Book> booksFound=new ArrayList<Book>();
        try {
            fis = new FileInputStream(BOOK_XML);
            ListBook listBook= (ListBook) marshaller.unmarshal(new StreamSource(fis));
            for(int i=0;i<listBook.getBookList().size();i++){
                if((listBook.getBookList().get(i).getPrice()>=price)){
                    booksFound.add(listBook.getBookList().get(i));
                }
            }
            return  booksFound;

        } finally {
            fis.close();
        }
    }

    @Override
    public List<Book> intersection(List<Book> first, List<Book> second) {
        List<Book> result=new ArrayList<Book>();
        for(int i=0;i<first.size();i++){
            for(int j=0;j<second.size();j++){
                if (first.get(i).getId().equals(second.get(j).getId())){
                    result.add(first.get(i));
                }
            }
        }
        return result;
    }
}
