package com.bookstore.rest;

import com.bookstore.dao.UserDAO;
import com.bookstore.model.ListBook;
import com.bookstore.model.ListUser;
import com.bookstore.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.oxm.UnmarshallingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.io.IOException;
import java.util.List;

/**
 * Created by Corina on 23.04.2016.
 */
@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class UserController {
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private ListUser listUser;
    private static final String USER_XML="D:\\SD2016_30434_Catarau_Corina_Bookstore\\src\\users.xml";




    @RequestMapping(value = "/user", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<User> createUser(@RequestBody User user) {
        try {
            ListUser myUsers=userDAO.getAll(USER_XML);
            for(int i=0;i<myUsers.getUserList().size();i++){
                if(user.getId().equals(myUsers.getUserList().get(i).getId())){
                    return  new ResponseEntity<User>(HttpStatus.CONFLICT);
                }
            }
            myUsers.getUserList().add(user);
            userDAO.create(myUsers,USER_XML);

        }catch (UnmarshallingFailureException ex){
            listUser.getUserList().add(user);
            try {
                userDAO.create(listUser,USER_XML);
            } catch (Exception e) {
                return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
            }
        }catch (NullPointerException exp){
            listUser.getUserList().add(user);
            try {
                userDAO.create(listUser,USER_XML);
            } catch (Exception e) {
                return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders header = new HttpHeaders();
        header.add("Location", "/rest/users/" + String.valueOf(user.getId()));
        return new ResponseEntity<User>(user, header, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/users/{idUser}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<User> updateUser(@PathVariable("idUser") String idUser,  @RequestBody  User user) {
        try {
           userDAO.delete(idUser,USER_XML);
            ListUser updated=userDAO.getAll(USER_XML);
            updated.getUserList().add(user);
            userDAO.create(updated,USER_XML);

        } catch (Exception e) {
            return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<User>> getAll(Model model) {
        List<User> finalUsers;
        try {
           ListUser users=this.userDAO.getAll(USER_XML);
            finalUsers=users.getUserList();

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(finalUsers, HttpStatus.OK);
    }

    @RequestMapping(value = "/users/{idUser}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<User> getUserById(@PathVariable String idUser ){
        try {
            User user = userDAO.getUserById(idUser);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/users/name/{nameUser}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<User> getUserByName(@PathVariable String nameUser ){
        try {
            User user = userDAO.getUserByName(nameUser);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/users/{idUser}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<User> deleteUser(@PathVariable String idUser){
        try {
             userDAO.delete(idUser,USER_XML);
            return new ResponseEntity<>( HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
