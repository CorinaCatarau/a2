package com.bookstore.model;

/**
 * Created by Corina on 02.05.2016.
 */
public class ReportGeneratorFactory {



    public static ReportGenerator getReport(String  type) {
        if ("PDF".equals(type)){
            return  new PdfCreator();
        }else if("CSV".equals(type)){
            return  new CsvGenerator();
        }
        return  null;
    }
}
