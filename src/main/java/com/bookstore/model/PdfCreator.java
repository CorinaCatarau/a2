package com.bookstore.model;

import com.itextpdf.text.*;
import com.itextpdf.text.List;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.*;

@Component("PDF")
public class PdfCreator implements ReportGenerator{
    private static String FILE = "D:/SD2016_30434_Catarau_Corina_Bookstore/src/main/resources/";
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);


    @Override
    public void generateReport(int nr, String fileName, java.util.List<Book> books) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        String myFile=FILE+fileName+".pdf";
        PdfWriter.getInstance(document, new FileOutputStream(myFile));
        document.open();
        PdfPTable table = new PdfPTable(3);

        PdfPCell c1 = new PdfPCell(new Phrase("Book Title"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Author"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Quantity"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        table.setHeaderRows(1);

        for(int i=0;i<books.size();i++){
            table.addCell(books.get(i).getTitle());
            table.addCell(books.get(i).getAuthor());
            if(books.get(i).getQuantity()==0){
                table.addCell("OUT OF STOCK");
            }else {
                table.addCell(books.get(i).getQuantity()+"");
            }
            }

        document.add(table);
        document.close();
    }
}