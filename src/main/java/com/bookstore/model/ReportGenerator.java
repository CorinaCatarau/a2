package com.bookstore.model;

import com.itextpdf.text.DocumentException;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by Corina on 02.05.2016.
 */
public interface ReportGenerator {
    public void generateReport(int nr,String fileName,List<Book> books) throws FileNotFoundException, DocumentException;
}
