package com.bookstore.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Corina on 23.04.2016.
 */

@XmlRootElement
public class User {

    private String id;
    private String name;
    private String password;
    private String role;

    public String getId() {return id;}
    @XmlElement(name = "id")
    public void setId(String id) {this.id = id;}
    public String getName() {return name;}
    @XmlElement(name = "name")
    public void setName(String name) {this.name = name;}
    public String getPassword() {return password;}
    @XmlElement(name = "password")
    public void setPassword(String password) {this.password = password;}
    public String getRole() {return role;}
    @XmlElement(name = "USER_ROLE")
    public void setRole(String role) {this.role = role;}
}
