<div class="container">
    <div class="row">
        <md-input-container>
            <label>Report Type</label>
            <md-select  ng-model="reportType" >
                <md-option  >PDF</md-option>
                <md-option  >CSV</md-option>
            </md-select>
        </md-input-container>
        <md-button class="lavender"  ng-click="generateReport(reportType)">GENERATE REPORT</md-button>
        <md-button class="lavender" ng-model="collapsed" ng-click="collapsed=!collapsed">ADD BOOK</md-button>
    </div>
<div class="row">

    <div ng-show="collapsed">
        <form name="bookFormCreate" ng-submit="createBook(newBook,$event)" class="myForm">
            <md-input-container class="md-block">
                <label>Author</label>
                <input  required="" md-no-asterisk="" name="newBookAuthor" ng-model="newBook.author">
                <div ng-messages="bookFormCreate.newBookAuthor.$error">
                    <div ng-message="required">This is required.</div>
                </div>
            </md-input-container>

            <md-input-container>
                <label>Genre</label>
                <md-select placeholder="Book genre" ng-model="newBook.genre" >
                    <md-option>COMEDY</md-option>
                    <md-option>DRAMA</md-option>
                    <md-option>NON-FICTION</md-option>
                    <md-option>FICTION</md-option>
                    <md-option>ROMANCE</md-option>
                    <md-option>TRAGEDY</md-option>
                    <md-option>HORROR</md-option>
                </md-select>
            </md-input-container>

            <md-input-container class="md-block">
                <label>Image Path</label>
                <input required="" name="newBookPath" ng-model="newBook.image" >
                <div ng-messages="bookFormCreate.newBookPath.$error"  >
                    <div ng-message="required">This is required.</div>
                </div>
            </md-input-container>

            <md-input-container class="md-icon-float md-icon-right md-block">
                <label>Price</label>
                <input  min="0" type="number" ng-model="newBook.price" >
            </md-input-container>

            <md-input-container class="md-icon-float md-icon-right md-block">
                <label>Quantity</label>
                <input  min="0" type="number" ng-model="newBook.quantity" >
            </md-input-container>

            <md-input-container class="md-block">
                <label>Title</label>
                <input  required="" md-no-asterisk="" name="newBookTitle" ng-model="newBook.title">
                <div ng-messages="bookFormCreate.newBookTitle.$error">
                    <div ng-message="required">This is required.</div>
                </div>
            </md-input-container>


            <md-button type="submit" id="submit" class="lavender" ng-disabled="bookFormCreate.$invalid">Submit</md-button>

        </form>
    </div>
    <div  ng-repeat="book in books" class="col-xs-2 " class="media">
        <div class="media-left">
            <a href="#">
                <a href=""> <img data-ng-src="data:image/JPEG;base64,{{book.image}}" class="img-responsive book"></a>
            </a>
        </div>
        <div class="media-body" ng-if="!editModeBook($index)">
            <ul class="list-group">
                <li class="list-group-item">
                    <md-button >
                        <i class="material-icons"  class="md-secondary md-hue-3" ng-click="setEditModeBook($index)">mode_edit</i>
                    </md-button>
                    <md-button >
                        <i class="material-icons"  class="md-secondary" ng-click="deleteBook(book,books)">delete_forever</i>
                    </md-button>
                </li>
                <li class="list-group-item">Title:{{book.title}}</li>
                <li class="list-group-item">Genre:{{book.genre}}</li>
                <li class="list-group-item">Price:{{book.price}}</li>

            </ul>
        </div>

        <div class="media-body"  ng-if="editModeBook($index)">
            <md-button >
                <i class="material-icons" ng-click="updateBook(book,$event)"  >arrow_forward</i>
            </md-button>
            <ul class="list-group">
                <li class="list-group-item">
                    <md-input-container class="md-block">
                        <label>Author</label>
                        <input  required="" md-no-asterisk="" name="bookAuthor" ng-model="book.author">
                        <div ng-messages="bookForm.bookAuthor.$error">
                            <div ng-message="required">This is required.</div>
                        </div>
                    </md-input-container>
                </li>
                <li class="list-group-item">
                    <md-input-container>
                        <label>Genre</label>
                        <md-select placeholder="Book genre" ng-model="book.genre" >
                            <md-option>COMEDY</md-option>
                            <md-option>DRAMA</md-option>
                            <md-option>NON-FICTION</md-option>
                            <md-option>FICTION</md-option>
                            <md-option>ROMANCE</md-option>
                            <md-option>TRAGEDY</md-option>
                            <md-option>HORROR</md-option>
                        </md-select>
                    </md-input-container>
                </li>

                <li class="list-group-item">
                    <md-input-container class="md-icon-float md-icon-right md-block">
                        <label>Price</label>
                        <input  min="0" type="number" ng-model="book.price" >
                    </md-input-container>
                </li>
                <li class="list-group-item">
                    <md-input-container class="md-icon-float md-icon-right md-block">
                        <label>Quantity</label>
                        <input  min="0" type="number" ng-model="book.quantity" >
                    </md-input-container>
                </li>
                <li class="list-group-item">
                    <md-input-container class="md-block">
                        <label>Title</label>
                        <input  required="" md-no-asterisk="" name="newBookTitle" ng-model="book.title">
                        <div ng-messages="bookFormCreate.newBookTitle.$error">
                            <div ng-message="required">This is required.</div>
                        </div>
                    </md-input-container>
                </li>


            </ul>

        </div>
    </div>


</div>
</div>