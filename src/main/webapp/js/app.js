/**
 * Created by Corina on 26.04.2016.
 */
var app = angular.module('app', ['ngMaterial','ngMessages']);
app.directive('toolbar', function() {
    return {
        restrict: 'E'
        ,
        replace: true,
        templateUrl: 'navbar'
    };
});



app .controller('AppCtrl',['$scope','$http','$mdDialog','$mdMedia','$window', function($scope,$http, $mdDialog, $mdMedia,$window){
    $scope.username;
    $scope.books;
    $scope.allBooks;
    $scope.advanced=false;
    $scope.amountOfInterval=0;

    console.log("${username}");
    console.log("<%=username%>");

   /* $scope.getData = function(username) {
        $http({
            method: 'GET',
            url: '/rest/users/username/'+username
        }).then(function successCallback(data) {
            $scope.getActivities(data.data.id);
            $window.localStorage.setItem("userId",data.data.id);
        }, function errorCallback(data) {
            console.log("error");
        });
    };*/



    $scope.getBooks = function() {
        $http({
            method: 'GET',
            url: '/rest/books/'
        }).then(function successCallback(data) {
            $scope.books=data.data;
            for (var i=0;i<$scope.books.length;i++){
               $scope.getImage($scope.books[i].id,i);
            }
            $scope.allBooks=angular.copy($scope.books);
        }, function errorCallback(data) {
            console.log("error");
        });
    }();


    $scope.multiSearch=function(author,genre,title,start,end){
        if(author ===undefined){
            author="";
        }
        if(genre ===undefined){
            genre="";
        }

        if(start ===null ||(start ===undefined)){
            start="";
        }
        if(end ===null ||(end ===undefined)){
            end="";
        }
            $http({
                method: 'GET',
                url: '/rest/books/filter?author='+author+'&genre='+genre+'&start='+start+'&end='+end
            }).then(function successCallback(data) {
                $scope.books=data.data;
                for (var i=0;i<$scope.books.length;i++){
                    $scope.getImage($scope.books[i].id,i);
                }
            }, function errorCallback(data) {
                console.log("error");
            });


    }


    $scope.byAuthor = function(author) {
        $http({
            method: 'GET',
            url: '/rest/books/author/'+author
        }).then(function successCallback(data) {
            $scope.books=data.data;
            for (var i=0;i<$scope.books.length;i++){
                $scope.getImage($scope.books[i].id,i);
            }
        }, function errorCallback(data) {
            console.log("error");
        });
    };

    $scope.byGenre = function(genre) {
        $http({
            method: 'GET',
            url: '/rest/books/genre/'+genre
        }).then(function successCallback(data) {
            $scope.books=data.data;
            for (var i=0;i<$scope.books.length;i++){
                $scope.getImage($scope.books[i].id,i);
            }
        }, function errorCallback(data) {
            console.log("error");
        });
    };

    $scope.byTitle = function(name) {
        $http({
            method: 'GET',
            url: '/rest/books/name/'+name
        }).then(function successCallback(data) {
            $scope.books=data.data;
            for (var i=0;i<$scope.books.length;i++){
                $scope.getImage($scope.books[i].id,i);
            }
        }, function errorCallback(data) {
            console.log("error");
        });
    };

    $scope.byPrice = function(start,end) {
        if(start === undefined){
            start=0;
        }
        if(end === undefined){
            end=0;
        }
        $http({
            method: 'GET',
            url: '/rest/books/price?start='+start+'&end='+end
        }).then(function successCallback(data) {
            $scope.books=data.data;
            for (var i=0;i<$scope.books.length;i++){
                $scope.getImage($scope.books[i].id,i);
            }
        }, function errorCallback(data) {
            console.log("error");
        });
    };

    $scope.getAuthors = function() {
        $http({
            method: 'GET',
            url: '/rest/books/authors'
        }).then(function successCallback(data) {
            $scope.authors=data.data;

        }, function errorCallback(data) {
            console.log("error");
        });
    }();



    $scope.getTitles = function() {
        $http({
            method: 'GET',
            url: '/rest/books/titles'
        }).then(function successCallback(data) {
            $scope.titles=data.data;

        }, function errorCallback(data) {
            console.log("error");
        });
    }();

    $scope.getGenres = function() {
        $http({
            method: 'GET',
            url: '/rest/books/genres'
        }).then(function successCallback(data) {
            $scope.genres=data.data;

        }, function errorCallback(data) {
            console.log("error");
        });
    }();


    $scope.getImage = function(idBook,i) {
        $http({
            method: 'GET',
            url: '/rest/book/'+idBook+'/image'
        }).then(function successCallback(data) {
           $scope.books[i].image=data.data;
        }, function errorCallback(data) {
            console.log("error");
        });
    };

    $scope.getQuantity=function(book,value){
        if (book.quantity>value){
            $scope.amountOfInterval++;
        }else{
            $scope.amountOfInterval=0;
        }
    }
    $scope.showAlertSuccess = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Success')
                .textContent('Selling complete')
                .ariaLabel('Alert Dialog Demo')
                .ok('Ok!')
                .targetEvent(ev)
        );
    };

    $scope.showInvalidAmount = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Error')
                .textContent('Amount invalid')
                .ariaLabel('Alert Dialog Demo')
                .ok('Ok!')
                .targetEvent(ev)
        );
    };


    $scope.sellBook=function(book,amount,event){
        $http({
            method: 'PUT',
            url: '/rest/books/sell?q='+amount+'&id='+book.id,
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(function successCallback(data) {
            console.log("Selling succesfull");
            $scope.getBooks;
            $scope.showAlertSuccess(event);

        }, function errorCallback(data) {

            $scope.showInvalidAmount(event);
            console.log("error at account update");
        });

    }
    $scope.hideSearch=function(){
        $scope.advanced=!$scope.advanced;
    }
    $scope.clearSearch=function(){
        $scope.books=$scope.allBooks;
        for (var i=0;i<$scope.books.length;i++){
            $scope.getImage($scope.books[i].id,i);
        }
    }


}]);































