package com.bookstore.dao;

import com.bookstore.model.Book;
import com.bookstore.model.ListBook;
import com.bookstore.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.asm.Label;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.ModelMap;

import javax.management.Query;
import javax.xml.transform.stream.StreamSource;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ModelMap;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertSame;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

/**
 * Created by Corina on 05.05.2016.
 */
@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
public class BookDAOTest {
    @Mock
    private BookDAO bookDAO;
    @Mock
    private Jaxb2Marshaller marshaller;
    @Mock
    FileInputStream fis;


    @Spy
    List<Book> books = new ArrayList<Book>();
    private MockMvc mockMvc;

    @Spy
    ModelMap model;

    private String group;
    Book myBook;
    Book b1=new Book();

    @Before
    public void prepare() throws Exception {
        b1.setQuantity(2);
        b1.setId("2");
        b1.setAuthor("Auth");
        b1.setGenre("test");
        b1.setImage("img.jpg");
        b1.setPrice(10);
        b1.setTitle("test");
        books.add(b1);

    }

    @Test
    public void findByName() throws Exception  {
        when(marshaller.unmarshal(new StreamSource(fis))).thenReturn(b1);
        when(bookDAO.getBookByName(anyString())).thenReturn(books);
        assertSame("test",bookDAO.getBookByName("test").get(0).getTitle());

    }

    @Test
    public void findAll() throws  Exception{
        when(marshaller.unmarshal(new StreamSource(fis))).thenReturn(b1);
        ListBook myList=new ListBook();
        myList.setBookList(books);
        when(bookDAO.getAll(anyString())).thenReturn(myList);
        assertSame("test",bookDAO.getAll(anyString()).getBookList().get(0).getTitle());
    }




}
